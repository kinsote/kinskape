import React from 'react';
import HeaderGame from './components/HeaderGame';
import MainRanking from './components/MainRanking';
import MainScore from './components/MainScore';
import './assets/css/Ranking.css';

const Ranking = () => {
	return (
		<div className="home">
			<header className="header">
				<HeaderGame />
			</header>
			<div className="contenedor-rank">
				<section>
					<MainRanking />
				</section>
				<section>
					<MainScore />
				</section>
			</div>
		</div>
	);
};
export default Ranking;
