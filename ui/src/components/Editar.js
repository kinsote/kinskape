import React, { useState } from 'react';
import { useSelector } from 'react-redux'; 
import '../assets/css/EditarUsuario.css';
import moment from 'moment';

const Editar = () => {
	const users = JSON.parse(localStorage.getItem('user'));
	const token = useSelector((s) => s.user && s.user.token);
	
	const [ user, setUser ] = useState(users);

	if (!user) return 'Cargando ...';

	const handleField = (field) => (e) => setUser({ ...user, [field]: e.target.value });

	const handleSave = (e) => {
		e.preventDefault();

		fetch(`http://localhost:8080/editUser`, {    ///:id
			method: 'PUT',
			headers: {
				Authorization: 'Bearer ' + token,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(user)
		})
			.then(() => {
				
				//console.log(user)
				//alert('Guardado correctamente!');
				window.location= 'http://localhost:3000/'
				
			})
			.catch(() => alert('Error guardando...'));
	};


	return (
		<form onSubmit={handleSave} className="conten-edit">
			<h1 className="tituloedit">Editar usuario </h1>
			<div className="edituser">
				<div>
					<label className="label-edit">
						Usuario: <br />
						<input value={user.name} onChange={handleField('name')} />
					</label>

					{/* <label className="label-edit">
                        Contraseña: <br /><input defaultValue type='password' onChange={handleField('password')} />
                    </label> */}

					<label className="label-edit">
						Cumpleaños: <br /> <input value={moment (user.birthday).format('DD-MM-YYYY')} onChange={handleField('birthday')} />
					</label>

					<label className="label-edit">
						Email: <br /> <input value={user.email} onChange={handleField('email')} />
					</label>

					<label className="label-edit">
						Avatar: <br /> <input value={user.avatar} onChange={handleField('avatar')} />
					</label>

					<button className="button-edit">Guardar</button>
				</div>
			</div>
		</form>
	);
};

export default Editar;
