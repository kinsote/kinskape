import React, { useState, useEffect } from 'react';


const MainScore = () => {
    
    const [ ranking, setRanking ] = useState({});

	useEffect(() => {
		fetch('http://localhost:8080/score?game=QUIZ', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then((res) => res.json())
			.then((res) => setRanking({ ranking: res }));
	}, []);

	const renderTableData = () => {
		if (Array.isArray(ranking.ranking)) {
			//console.log(ranking);

			return ranking.ranking.map((rank, index) => {
				const { id, username, score } = rank;
				return (
					<tr key={id}>
						<td>{username}</td>
						<td>{score}</td>
					</tr>
				);
			});
		} else {
			return (
				<tr>
					<td />
					<td />
				</tr>
			);
		}
	};

	return (
		<section className="score">Quiz
			<table className="tabla-rank">
				<thead>
					<tr className="user-rank">
						<th>Nombre</th>
						<th>Aciertos</th>
					</tr>
				</thead>
				<tbody className="data-rank">{renderTableData()}</tbody>
			</table>

			<div className="clearfix" />
		</section>
	);
	
};
export default MainScore;



	