export const quizData = [
  {
    id: 1,
    question: `¿Cómo se llama la víctima real de la película "El Exorcismo de Emily Rose"?`,
    options1: [`Regan Teresa MacNell`, `Anneliese Michel`, `Mia Farrow`],
    answer: `Anneliese Michel`  },
  {
    id: 2,
    question: `Que actor hacia de si mismo en bienvenidos a Zombieland?`,
    options1: [`Bill Murray`, `Woody Harrelson`, `Elon Musk`],
    answer: `Bill Murray`
  },
  {
    id: 3,
    question: `De que año es Zombie el regreso de los Muertos Vivientes?`,
    options1: [`1973`, `1978`, `1983`],
    answer: `1978`
  },  
  {
    id: 4,
    question: `¿Quién es el director de "El resplandor"?`,
    options1: [`Roman Polanski`, `Alfred HitchCock`, `Stanley Kubrick`],
    answer: `Stanley Kubrick`
  },
  {
    id: 5,
    question: ` Donde se desarrolla zombies Nazis ?`,
    options1: [`Alemania`, `Belgica`, `Francia`],
    answer: `Alemania`  
  },
  {
    id: 6,
    question: `La famosa frase "I see dead people" (a veces veo muertos) ¿De qué película es?`,
    options1: [`Suspira`, `El Sexto Sentido`, `Poltergeis`],
    answer: `El Sexto Sentido`
  },
  {
    id: 7,
    question: `Cuál es el nombre del muñeco de "Chucky"?`,
    options1: [`Charles Raynold`, `Charlie`, `Charles Lee ray`],
    answer: `Charles Lee ray`
  }
];